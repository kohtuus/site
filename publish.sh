#!/usr/bin/env bash

rm -rf build
npm run build
rm -rf tmp
git clone -b pages git@codeberg.org:kohtuus/site.git tmp
rm -rf tmp/_app
rm tmp/*
cp -R build/* tmp
cd tmp
git add .
git commit -a -m "Update"
git push origin pages
cd ..
